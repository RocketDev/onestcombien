# onestcombien

Web Application for people counting

## Getting started

Set configuration of your database on .env file. 

Some commands need to be run to work with this project : 

#### Create database

`php bin/console doctrine:database:create`

#### Make migration

`php bin/console make:migration`

#### Install migrations

`php bin/console doctrine:migrations:migrate`

## Server
If needed you can run development server by : `symfony server:start`

# Production

Get this project and install

```
git clone https://gitlab.com/RocketDev/onestcombien
cd onestcombien
php bin/console doctrine:database:create
php bin/console doctrine:migrations:migrate
composer install
npm install
composer dump-env prod
npm run build
```
