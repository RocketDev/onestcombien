<?php

namespace App\Repository;

use App\Entity\Vote;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Vote|null find($id, $lockMode = null, $lockVersion = null)
 * @method Vote|null findOneBy(array $criteria, array $orderBy = null)
 * @method Vote[]    findAll()
 * @method Vote[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VoteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Vote::class);
    }

    /**
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\NoResultException
     */
    public function findVoteOfTodayByUid($Uid)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.uid = :uid')
            ->andWhere('v.date = :date')
            ->setParameter('uid', $Uid)
            ->setParameter('date', date('Y-m-d'))
            ->setMaxResults(1)
            ->getQuery()
            ->getSingleResult()
            ;
    }

    /**
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\NoResultException
     */
    public function findVoteOfTodayByIp($ip)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.anon_ip = :ip')
            ->andWhere('v.date = :date')
            ->setParameter('ip', hash("sha512", $ip.date('Y/m/d')))
            ->setParameter('date', date('Y-m-d'))
            ->setMaxResults(1)
            ->getQuery()
            ->getSingleResult()
            ;
    }

    /**
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\NoResultException
     */
    public function countVoteInSameCity($cityId)
    {
        return $this->createQueryBuilder('v')
            ->select('count(v.id)')
            ->andWhere('v.city = :city')
            ->andWhere('v.date = :date')
            //->andWhere('v.similary = FALSE')
            ->setParameter('city', $cityId)
            ->setParameter('date', date('Y-m-d'))
            ->setMaxResults(1)
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\NoResultException
     */
    public function countTotalVoteForToday($today = true)
    {
        $date = date('Y-m-d', strtotime(($today)?"now":"yesterday"));
        return $this->createQueryBuilder('v')
            ->select('count(v.id)')
            ->andWhere('v.date = :date')
            //->andWhere('v.similary = FALSE')
            ->setParameter('date', $date)
            ->setMaxResults(1)
            ->getQuery()
            ->getSingleScalarResult();
    }
}
