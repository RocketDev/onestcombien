<?php

namespace App\Repository;

use App\Entity\City;
use App\Entity\Vote;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method City|null find($id, $lockMode = null, $lockVersion = null)
 * @method City|null findOneBy(array $criteria, array $orderBy = null)
 * @method City[]    findAll()
 * @method City[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CityRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, City::class);
    }

    public function findBySearch($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.name LIKE :val')
            ->setParameter('val', $value.'%')
            ->orWhere('c.slug LIKE :slug')
            ->setParameter('slug', strtolower($value).'%')
            ->orderBy('c.slug', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getArrayResult()
            ;
    }

    /**
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\NoResultException
     */
    public function cityExist($name, $slug = null)
    {
        $query = $this->createQueryBuilder('c')
            ->andWhere('c.name = :name')
            ->setParameter('name', $name);
        if($slug) {
            $query->orWhere('c.slug = :slug')
                ->setParameter('slug', $slug);
        }

        return $query->setMaxResults(1)
            ->getQuery()
            ->getSingleResult();
    }

    public function getResultsGroupByCity($today = true)
    {
        $date = date('Y-m-d', strtotime(($today)?"now":"yesterday"));
        return $this->createQueryBuilder('c')
            ->select('c.name, count(c.id) as nbManifestants')
            ->innerJoin(Vote::class, 'v', 'WITH', 'c.id = v.city')
            ->where('v.date = :date')
            //->andWhere('v.similary = FALSE')
            ->setParameter('date', $date)
            ->groupBy('c.name')
            ->getQuery()
            ->getArrayResult();
    }
}
