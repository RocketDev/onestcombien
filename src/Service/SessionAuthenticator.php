<?php

namespace App\Service;

use App\Entity\User;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RequestStack;

class SessionAuthenticator extends AbstractController
{
    private User $user;

    public function __construct(RequestStack $requestStack) {
        $this->user = new User();
        if ($requestStack->getCurrentRequest()) {
            $uid = $requestStack->getCurrentRequest()->cookies->get('onestcombien_uid');
            $this->user->setUid($uid);

            $lastCity = $requestStack->getCurrentRequest()->cookies->get('onestcombien_last_city');
            $this->user->setLastCity($lastCity);
        }
    }

    public function getCurrentUser() : User
    {
        return $this->user;
    }

    public function isNewUser() : bool
    {
        return $this->getCurrentUser()->getUid() === null;
    }
}