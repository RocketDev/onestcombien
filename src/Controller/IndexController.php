<?php

namespace App\Controller;

use App\Repository\CityRepository;
use App\Repository\VoteRepository;
use Psr\Cache\InvalidArgumentException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Cache\CacheInterface;

class IndexController extends AbstractController
{

    private CityRepository $cityRepository;
    private VoteRepository $voteRepository;
    private ?bool $canVote = null;
    private CacheInterface $cache;

    public function __construct(CityRepository $cityRepository, VoteRepository $voteRepository, CacheInterface $cache)
    {
        $this->cityRepository = $cityRepository;
        $this->voteRepository = $voteRepository;
        $this->cache = $cache;
    }

    /**
     * @Route("/", name="index")
     */
    public function index(): Response
    {
        $this->setCanVote();
        $data['canVote'] = $this->canVote;
        $data['resultLastDay'] = (date('H') <= $this->getParameter('hours_start_export'));

        //Si on a les résultats, on va compter globalement
        if(!$this->canVote) {
            $data['nbVotes'] = $this->setCacheOfResult("result_manifestation_total_", "voteRepository", "countTotalVoteForToday");
        }

        return $this->render('index/index.html.twig', $data);
    }

    /**
     * @Route("/mentions", name="mentions")
     */
    public function mentions(): Response
    {
        //TODO : créer un formulaire de contact
        return $this->render('index/mentions.html.twig', [
            'controller_name' => 'IndexController',
        ]);
    }

    /**
     * @Route("/resultats", name="resultats")
     */
    public function resultats(): Response
    {
        $this->setCanVote();

        $data['canVote'] = $this->canVote;
        if(!$this->canVote) {
            $data['results'] = $this->setCacheOfResult("result_manifestation_by_city_", "cityRepository", "getResultsGroupByCity", []);
        }

        return $this->render('index/resultats.html.twig', $data);
    }

    /**
     * @Route("/resultats_export", name="resultats_export")
     */
    public function exportResult() : Response
    {
        $this->setCanVote();
        $results=[];
        if(!$this->canVote) {
            $results = $this->setCacheOfResult("result_manifestation_by_city_", "cityRepository", "getResultsGroupByCity", []);
        }

        $response = new StreamedResponse();
        $response->setCallback(function() use ($results) {
            if ($results) {
                array_unshift($results, array_keys(reset($results)));
                $outputBuffer = fopen("php://output", 'w');
                foreach($results as $v) {
                    fputcsv($outputBuffer, $v);
                }
                fclose($outputBuffer);
            }
        });

        $response->headers->set("Content-Type", "text/csv; charset=utf-8");
        $dateKey = date('d/m/Y');
        if (date('H') <= $this->getParameter('hours_start_export')) {
            $dateKey = date('d/m/Y', strtotime('-1 day'));
        }
        $filename = "resultatsManifestation_".$dateKey.'.csv';
        $response->headers->set("Content-Disposition", "attachment; filename={$filename}");
        return $response;
    }

    private function setCanVote() : void
    {
        if (is_null($this->canVote)) {
            $currentHours = date('H');
            $this->canVote = ($currentHours >= $this->getParameter('hours_start_export') && $currentHours <= $this->getParameter('hours_end_export'));
        }
    }

    private function setCacheOfResult(string $key, string $repository, string $method, $default = 0)
    {
        $dateKey = date('d-m');
        $today = true;
        if (date('H') <= $this->getParameter('hours_start_export')) {
            $today = false;
            $dateKey = date('d-m', strtotime('-1 day'));
        }

        try {
            return $this->cache->get($key.$dateKey, function() use ($today, $default, $method, $repository) {
                $countVote = $this->$repository->$method($today);
                if($countVote)
                    return $countVote;
                return $default;
            });
        } catch (\Exception | InvalidArgumentException $e) {}

        return $default;
    }
}
