<?php

namespace App\Controller;

use App\Entity\Vote;
use App\Form\VoteType;
use App\Repository\CityRepository;
use App\Repository\VoteRepository;
use App\Service\SessionAuthenticator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Annotation\Route;

class VoteController extends AbstractController
{
    private CityRepository $cityRepository;
    private VoteRepository $voteRepository;

    private string $environnement;
    private ?string $ip;
    private array $cookies = [];

    public function __construct(CityRepository $cityRepository, VoteRepository $voteRepository, KernelInterface $kernel, RequestStack $requestStack)
    {
        $this->cityRepository = $cityRepository;
        $this->voteRepository = $voteRepository;
        $this->environnement = $kernel->getEnvironment();
        $this->ip = ($this->environnement === "dev")?"127.0.0.1":$requestStack->getCurrentRequest()->getClientIp();
    }

    /**
     * @Route("/vote", name="vote")
     */
    public function index(Request $request, SessionAuthenticator $sessionAuthenticator): Response
    {
        //Si on est au déla de l'heure défini on peut plus voter
        $currentHours = date('H');
        $canVote = ($currentHours >= $this->getParameter('hours_start_export') && $currentHours <= $this->getParameter('hours_end_export'));
        if (!$canVote) {
            $data['cantVote'] = true;
            return $this->makeReturnVote($data);
        }

        $data = [];
        $uid = $sessionAuthenticator->getCurrentUser()->getUid();
        $vote = null;
        $similaryIp = false;

        //On vérifie si on trouve un vote via un cookie
        if (!$sessionAuthenticator->isNewUser()) {
            try {
                $vote = $this->voteRepository->findVoteOfTodayByUid($uid);
                //Si on a retrouvé un vote on peut le retourner
                if ($vote) {
                    return $this->mappedDataAndReturn($data, $vote);
                }
            } catch (\Exception $e) {}
        }

        //Sinon on vérifie si l'ip anonymisé + date du jour n'a pas déjà voté
        if(is_null($vote)) {
            try {
                $vote = $this->voteRepository->findVoteOfTodayByIp($this->ip);
                if($vote)
                    $similaryIp = true;
            } catch (\Exception $e) {}
        }

        $vote = new Vote();
        $vote->setSimilary($similaryIp);
        $form = $this->createForm(VoteType::class, $vote);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $voteDataRequest = $request->request->get('vote');

            $cityName = array_key_exists("city_name", (array)$voteDataRequest)?$voteDataRequest['city_name']:null;
            $slug = strtolower($cityName);
            $city = null;
            try {
                $city = $this->cityRepository->cityExist($cityName, $slug);
            } catch(\Exception $e) {}

            if ($city) {
                $this->saveVote($vote, $city, $uid);
                $response = $this->redirectToRoute('vote');
                $this->addFlash("success", "Inscription bien prise en compte");
                $this->setCookiesInHeaders($response);
                return $response;
            }

            $this->addFlash("error", "La ville n'a pas pu être retrouvé dans notre base de donnée. Merci de vérifier l'orthographe.");
            $data['error'] = true;
        }

        $data['form'] = $form->createView();

        return $this->makeReturnVote($data);
    }

    private function setCookiesInHeaders(Response $response)
    {
        if(!empty($this->cookies)) {
            foreach ($this->cookies as $cookie) {
                $response->headers->setCookie($cookie);
            }
        }
    }

    private function makeReturnVote(array $data = []) : Response
    {
        $response = $this->render('vote/index.html.twig', $data);
        $this->setCookiesInHeaders($response);
        return $response;
    }

    private function mappedDataAndReturn(array $data, Vote $vote): Response
    {
        $data['vote'] = $vote;
        $nbVoteInSameCity = 0;
        try {
            $nbVoteInSameCity = $this->voteRepository->countVoteInSameCity($vote->getCity()->getId());
        } catch (\Exception $e) {}
        $data['nb_sameplace'] = $nbVoteInSameCity;
        return $this->makeReturnVote($data);
    }

    private function saveVote($vote, $city, $uid) : void
    {
        $anonIp = hash("sha512", $this->ip.date('Y/m/d'));
        if(!$uid)
            $uid = bin2hex(openssl_random_pseudo_bytes(40));

        $vote->setUid($uid);
        $vote->setAnonIp($anonIp);
        $vote->setCity($city);
        $vote->setDate(new \DateTime("now"));

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($vote);
        $entityManager->flush();
        $this->setCookies($uid, $city->getName());
    }

    private function setCookies(string $uid, string $cityName) : void
    {
        $this->cookies[] = Cookie::create("onestcombien_uid", $uid, new \DateTime('+1 year'));
        $this->cookies[] = Cookie::create("onestcombien_last_city", $cityName, new \DateTime('+1 year'));
    }

    /**
     * @Route("/search_city", name="search_city", methods={"GET"})
     */
    public function search(Request $request)
    {
        $query = $request->query->get('search');
        $results = $this->cityRepository->findBySearch($query);
        return new JsonResponse($results);
    }
}
