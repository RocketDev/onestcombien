<?php

namespace App\Entity;

use App\Repository\VoteRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=VoteRepository::class)
 * @ORM\Table(name="vote", indexes={
 *     @ORM\Index(name="anon_ip_date", columns={"anon_ip", "date"})
 * })
 */
class Vote
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $anon_ip;

    /**
     * @ORM\Column(type="date")
     */
    private ?\DateTimeInterface $date;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $uid;

    /**
     * @ORM\ManyToOne(targetEntity=City::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private ?City $city;

    /**
     * @ORM\Column(type="boolean")
     */
    private $similary;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAnonIp(): ?string
    {
        return $this->anon_ip;
    }

    public function setAnonIp(string $anon_ip): self
    {
        $this->anon_ip = $anon_ip;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getUid(): ?string
    {
        return $this->uid;
    }

    public function setUid(string $uid): self
    {
        $this->uid = $uid;

        return $this;
    }

    public function getCity(): ?City
    {
        return $this->city;
    }

    public function setCity(?City $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getSimilary(): ?bool
    {
        return $this->similary;
    }

    public function setSimilary(bool $similary): self
    {
        $this->similary = $similary;

        return $this;
    }
}
