<?php

namespace App\Entity;

class User
{
    private ?string $uid;
    private ?string $lastCity;

    public function setUid(?string $uid) : void
    {
        $this->uid = $uid;
    }

    public function getUid() : ?string
    {
        return $this->uid;
    }

    public function setLastCity(?string $city) : void
    {
        $this->lastCity = $city;
    }

    public function getLastCity() : ?string
    {
        return $this->lastCity;
    }
}
