import jquery from 'jquery';
import * as Bloodhound from 'corejs-typeahead';

jquery(document).ready(function () {
    let cities = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace("name"),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        limit: 10,
        remote: {
            url: './search_city?search=%QUERY%',
            wildcard: '%QUERY%',
            filter : function(response) { return response; }
        }
    });

    cities.initialize();

    jquery('#vote_city_name').typeahead({
        hint : true,
        highlight : true,
        minLength : 2,
        autoselect : true
    }, {
        source: cities.ttAdapter(),
        displayKey: "name",
        name: "searchable-cities",
        templates: {
            empty: '<div class="empty-message">Aucune ville ne correspond à votre recherche</div>',
            suggestion: function(data) {
                return "<div class='result-search' data-id='"+data.id+"'><span>"+data.name+"</span></div>";
            }
        }
    });

    jquery("form[name='vote']").keypress(function(e) {
        if(e.which == 13) {
            e.preventDefault();
        }
    });
});

